import java.util.Arrays;
import java.util.Scanner;

public class LCSS {
	private String strOne;c
	private String strTwo;
	private int len1;
	private int len2;
	private int LCSSLen;
	private int[][] LCSSTable;
	private char[] comStr;
	public int timeCount = 0;

	public LCSS(String st1, String st2){
		strOne = st1;
		strTwo = st2;
		len1 = strOne.length();
		len2 = strTwo.length();
		LCSSTable = new int[len1 + 1][len2 + 1];

		for(int i = 0; i < len1; i++) {
			for(int j = 0; j < len2; j++){
				LCSSTable[i][j] = 0;
			}
		}
	}

	public int computeLCSS(){
		LCSSLen = 0;
		for(int i = 1; i < len1 + 1; i++) {
			for(int j = 1; j < len2 + 1; j++){
				if(strOne.charAt(i - 1) == strTwo.charAt(j - 1)){
					LCSSTable[i][j] = LCSSTable[i -1 ][j - 1] + 1;
				} else {
					LCSSTable[i][j] = (LCSSTable[i][j - 1] >LCSSTable[i - 1][j]) ?LCSSTable[i][j - 1]:LCSSTable[i - 1][j];
				}
				timeCount++;
			}
		}

		LCSSLen = LCSSTable[len1][len2];
		comStr = new char[LCSSLen + 1];
		comStr[LCSSLen] = '\0';
		int k = 1;

		for(int i = len1 ; i > 0 && (k <= LCSSLen); ) {
			for(int j = len2 ; j > 0 && (k <= LCSSLen);) {
				if(LCSSTable[i][j] != 0) {
					if(LCSSTable[i][j] == LCSSTable[i][j - 1]) {
							j--;
					} else {
						if(LCSSTable[i ][j ] == LCSSTable[i - 1][j ]) {
							i--;
						} else {
								comStr[LCSSLen - k] = strOne.charAt(i-1);
								k++;
								i--;
								j--;
						}
					}
				} else {
					break;
				}
			}
		}
		return LCSSLen;
	}

	public void printStrings() {
		System.out.println(Arrays.toString(comStr));
	}

	public int getLCSSLen() {
		return LCSSLen;
	}

	public int getTimeCount() {
        return timeCount;
	}

	public static void main(String[] args) {

		String stringOne, stringTwo;
		Scanner input = new Scanner(System.in);

		System.out.print("Enter the first text: ");
		stringOne = input.nextLine();

		System.out.print("Enter the second text: ");
		stringTwo = input.nextLine();

		LCSS object = new LCSS(stringOne, stringTwo);
	    object.computeLCSS();
		object.printStrings();
		int copied = object.getLCSSLen();
		System.out.println("Number of letters copied: " + copied);
		int length1 = stringOne.length();
		int length2 = stringTwo.length();
		System.out.println("Length 1: " + length1);
		System.out.println("Length 2: " + length2);

		// TODO: Percetage calculator
	}

}